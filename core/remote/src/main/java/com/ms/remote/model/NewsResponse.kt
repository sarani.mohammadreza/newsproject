package com.ms.remote.model

import com.google.gson.annotations.Expose
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class NewsResponse(

    @Json(name = "source")
    @Expose
    val source: Source,

    /**
     * Name of the author for the article
     */
    @Json(name = "author")
    @Expose
    val author: String? = null,

    /**
     * Title of the article
     */
    @Json(name = "title")
    @Expose
    val title: String? = null,

    /**
     * Complete description of the article
     */
    @Json(name = "description")
    @Expose
    val description: String? = null,

    /**
     * URL to the article
     */
    @Json(name = "url")
    @Expose
    val url: String? = null,

    /**
     * URL of the artwork shown with article
     */
    @Json(name = "urlToImage")
    @Expose
    val urlToImage: String? = null,

    /**
     * Date-time when the article was published
     */
    @Json(name = "publishedAt")
    @Expose
    val publishedAt: String? = null,

    @Json(name = "content")
    @Expose
    val content: String? = null
                      )
{
    data class Source(

        @Json(name = "id")
        @Expose
        val id: String? = null,

        @Json(name = "name")
        @Expose
        val name: String? = null
                     )
}
