package com.ms.remote.service

import com.ms.remote.model.NewsResponseList
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteService
{
    @GET("top-headlines?apiKey=307d5bf0b592481c8dc91159b63b387c&sources=techcrunch")
    suspend fun getTopHeadlines(
        @Query("page") page: Int = 0,
        @Query("pageSize") pageSize: Int = 10
    ): NewsResponseList
}