package com.ms.remote.model

import com.google.gson.annotations.Expose
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewsResponseList(

@Json(name = "status")
@Expose
val status: String = "",

@Json(name = "totalResults")
@Expose
val totalResults: Int = 0,

@Json(name = "articles")
@Expose
val articles: ArrayList<NewsResponse>

)

