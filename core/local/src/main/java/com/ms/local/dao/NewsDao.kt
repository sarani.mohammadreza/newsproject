package com.ms.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.ms.local.entity.NewsEntity


@Dao
interface NewsDao : BaseDao<NewsEntity> {

    @Query("DELETE FROM News_Table")
    suspend fun deleteAll()

    @Query("select * from News_Table")
    suspend fun newsList(): List<NewsEntity>

    @Query("select * from News_Table where id = :id")
    suspend fun newsById(id: Int): NewsEntity


}