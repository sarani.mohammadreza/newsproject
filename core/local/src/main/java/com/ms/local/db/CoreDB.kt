package com.ms.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ms.local.dao.NewsDao
import com.ms.local.entity.NewsEntity

@Database(
    entities = [NewsEntity::class],
    version = 1,
    exportSchema = false
)
abstract class CoreDB  : RoomDatabase() {
    abstract fun newsDao() : NewsDao
}