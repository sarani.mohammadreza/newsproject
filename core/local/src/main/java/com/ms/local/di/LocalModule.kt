package com.ms.local.di

import androidx.room.Room
import androidx.room.RoomDatabase
import com.ms.local.db.CoreDB
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


fun localModule(dbName : String) = module {

    single {
        Room.databaseBuilder(androidApplication() , CoreDB::class.java , dbName)
            .fallbackToDestructiveMigration()
            .build()
    }

    factory {
        get <CoreDB>().newsDao()
    }
}