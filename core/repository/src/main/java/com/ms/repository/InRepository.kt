package com.ms.repository

import com.ms.local.entity.NewsEntity
import com.ms.remote.model.NewsResponse
import com.ms.remote.model.NewsResponseList
import com.ms.remote.network.NetworkState
import kotlinx.coroutines.flow.Flow

interface InRepository
{
    // get all items From Server
    fun getNews(page:Int = 0 , pageSize : Int = 10) : Flow<NetworkState<NewsResponseList>>

    suspend fun insertDB (newsEntity: NewsEntity)

    suspend fun getItemsDB() : List<NewsEntity>
}