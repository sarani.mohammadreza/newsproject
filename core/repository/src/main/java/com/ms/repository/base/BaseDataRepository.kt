package com.ms.repository.base


import android.annotation.SuppressLint
import android.util.Log
import com.ms.remote.network.NetworkState
import com.ms.remote.network.RemoteDataException
import retrofit2.Response

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.delay
import java.io.IOException

abstract class BaseDataRepository {

    protected suspend fun <T : Any> apiCallResponse(call: suspend () -> Response<T>): NetworkState<T> {
        return try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null)
                    NetworkState.Success(body)
                else
                    NetworkState.Empty("Json Body Is Zero")
            } else
                NetworkState.Error(RemoteDataException(response.errorBody()))

        } catch (ex: Throwable) {
            NetworkState.Error(RemoteDataException(ex))
        }
    }

    protected suspend fun <T : Any> apiCall(call: suspend () -> T): NetworkState<T> {
        return try {
            val response = call()
            NetworkState.Success(response)
        } catch (ex: Throwable) {
            NetworkState.Error(RemoteDataException(ex))
        }
    }

    protected suspend fun <T : Any> apiCallRetry(
        times: Int = 3,
        call: suspend () -> T
    ): NetworkState<T> {
        return try {
            val response = apiCallIO(timesValue = times, block = call)
            NetworkState.Success(response)
        } catch (ex: Throwable) {
            NetworkState.Error(RemoteDataException(ex))
        }
    }

    @SuppressLint("LogNotTimber")
    protected suspend fun <T> apiCallIO(
        timesValue: Int = 4,
        initializeDelay: Long = 100, // 100 miliSecond
        maxDelay: Long = 1000, // 1 Second
        factor: Double = 2.0,
        block: suspend () -> T
    ): T {
        var currentDelay = initializeDelay
        repeat(timesValue - 1)
        {
            try {
                return block()
            } catch (e: IOException) {
                Log.e("MohammadReza", "Error Api Call IO")
            }
            delay(currentDelay)
            currentDelay = (currentDelay * factor).toLong().coerceAtMost(maxDelay) // checks
        }
        return block()
    }


    // for Deferred Item Inside  Suspend
    protected suspend inline fun <reified T : Any> apiCallDeferred(request: Deferred<Response<T>>): NetworkState<T> {
        return try {
            val response = request.await() // get Deferred Value From Coroutine Items
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null)
                    NetworkState.Success(body)
                else
                    NetworkState.Empty("Json Body Is Empty")
            } else
                NetworkState.Error(RemoteDataException(response.errorBody()))

        } catch (e: Throwable) {
            NetworkState.Error(RemoteDataException(e))
        }
    }
}