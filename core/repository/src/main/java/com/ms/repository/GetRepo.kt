package com.ms.repository

import com.ms.local.dao.NewsDao
import com.ms.local.entity.NewsEntity
import com.ms.remote.model.NewsResponse
import com.ms.remote.model.NewsResponseList
import com.ms.remote.network.NetworkState
import com.ms.remote.service.RemoteService
import com.ms.repository.base.BaseDataRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetRepo(
    private val service: RemoteService,
    private val db: NewsDao
    ) :  BaseDataRepository() , InRepository
{
    override fun getNews(page:Int, pageSize : Int): Flow<NetworkState<NewsResponseList>>  = flow {

        emit(apiCall{service.getTopHeadlines(page , pageSize)})
    }

    override suspend fun insertDB(newsEntity: NewsEntity)  = db.insert(newsEntity)

    override suspend fun getItemsDB(): List<NewsEntity>  = db.newsList()


    }