package com.ms.repository.di

import com.ms.repository.GetRepo
import com.ms.repository.InRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory {
        GetRepo(get() , get()) as InRepository
    }
}