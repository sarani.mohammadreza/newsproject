package com.ms.newsApp.base.recyclerview

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class RecyclerItemTouchListener(
    val context: Context,
    val rv: RecyclerView,
    val listener: OnItemClickListener
): RecyclerView.OnItemTouchListener
{
    lateinit var gesture: GestureDetector
    
    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean
    {
        if (gesture.onTouchEvent(e))
            return true
        
        return false
    }
    
    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent)
    {
    }
    
    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean)
    {
    }
    
    
    init
    {
        gesture = GestureDetector(context, object: GestureDetector.SimpleOnGestureListener()
        {
            override fun onSingleTapConfirmed(e: MotionEvent?): Boolean
            {
                val childView: View = e?.let { rv.findChildViewUnder(it.x, e.y) } !!
                listener.onItemClick(childView, rv.getChildAdapterPosition(childView))
                return true
            }
    
            override fun onDoubleTap(e: MotionEvent?): Boolean
            {
                val childView: View = e?.let { rv.findChildViewUnder(it.x, e.y) } !!
                listener.onDoubleClick(childView, rv.getChildAdapterPosition(childView))
                return true
            }
    
            override fun onLongPress(e: MotionEvent?)
            {
                val childView: View = e?.let { rv.findChildViewUnder(it.x, e.y) } !!
                listener.onLongItemClick(childView, rv.getChildAdapterPosition(childView))
            }
        })
    }
    
    
    interface OnItemClickListener
    {
        fun onItemClick(v: View, position: Int)
        
        fun onDoubleClick(v: View, position: Int)
        
        fun onLongItemClick(v: View, position: Int)
    }
}