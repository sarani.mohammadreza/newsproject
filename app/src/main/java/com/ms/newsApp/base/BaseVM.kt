package com.ms.newsApp.base

import androidx.lifecycle.ViewModel
import com.ms.repository.InRepository
import java.lang.ref.WeakReference

abstract class BaseVM<P>(val dataService: InRepository) : ViewModel() {

    private lateinit var presenter: WeakReference<P>

    fun getPresenter(): P? {
        return presenter.get()
    }


    fun setP(presenter: P) {
        this.presenter = WeakReference(presenter)
    }

}