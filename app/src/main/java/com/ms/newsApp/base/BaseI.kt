package com.ms.newsApp.base

interface BaseI {

    fun showLoading()
    fun hideLoading()
    fun showError(code: Int, message: String)
    fun showSuccess(title: String, message: String)

    fun initUI()
    fun initListener()
    fun initPresenter()
}