package com.ms.newsApp.base.recyclerview

import androidx.recyclerview.widget.RecyclerView
import java.util.*

abstract class BaseAdapter<T, E: BaseVH<T>>(): RecyclerView.Adapter<E>(), BaseActionAdapter<E>
{
    
    var items: ArrayList<T> = arrayListOf()
    
    constructor(items: ArrayList<T>): this()
    {
        this.items = items
    }
    
    override fun getItemCount(): Int = items.size
    
    fun addRangedView(items: List<T>)
    {
        this.items.addAll(items)
        notifyItemRangeInserted(this.items.size - items.size, items.size)
    }
    
    fun setData(items: List<T>)
    {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
    
    fun listSize() = items.size
    
    fun onClearItems()
    {
        this.items.clear()
    }
    
    override fun onBindViewHolder(holder: E, position: Int)
    {
        
        beforeBind(holder)
        
        holder.bind(items[position], position)
    }
    
    override fun beforeBind(holder: E)
    {
    }
}