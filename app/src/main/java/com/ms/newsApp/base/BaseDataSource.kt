package com.ms.newsApp.base

import androidx.paging.PageKeyedDataSource
import com.ms.repository.InRepository

abstract class BaseDataSource<T,S>(val dataService: InRepository) : PageKeyedDataSource<T, S>()