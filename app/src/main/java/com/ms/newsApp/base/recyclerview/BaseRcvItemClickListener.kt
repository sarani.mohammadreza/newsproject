package com.ms.newsApp.base.recyclerview

interface BaseRcvItemClickListener<T>
{
    fun onItemClick(item : T  , position : Int)
}