package com.ms.newsApp.base

import android.app.Activity
import android.os.Bundle
import android.view.*
import androidx.annotation.LayoutRes
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.ms.widget.CustomProgressBar
import com.ms.widget.CustomSnackBar
import com.ms.widget.SnackBarType
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

abstract class BaseFragment : Fragment(), BaseI
{

    private val progress: CustomProgressBar by lazy {
        CustomProgressBar(context = requireContext())
    }

    @get:LayoutRes
    protected abstract val layoutId: Int?

    override fun initUI()
    {
    }

    override fun initListener()
    {
    }

    override fun initPresenter()
    {
    }

    open fun initBinding(inflater: LayoutInflater, container: ViewGroup?): View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {

        return layoutId?.let {
            inflater.inflate(it, container, false)
        } ?: run {
            initBinding(inflater, container)
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        initPresenter()
        initUI()
        initListener()
    }

    override fun showLoading()
    {
        progress.toggle(status = true)
    }

    override fun hideLoading()
    {
        progress.release()
    }

    override fun showError(code: Int, message: String)
    {

    }


    override fun showSuccess(title: String, message: String)
    {

    }

    protected fun showSnackBar(
        snackBarType: SnackBarType, container: ViewGroup,
        messageTitle: String,
        messageSubTitle: String? = null,
        buttonText: String? = null,
        listener: CustomSnackBar.OnSnackBarActionListener? = null
    )
    {
        showSnackBar(
            snackBarType = snackBarType,
            container,
            messageTitle = messageTitle,
            messageSubTitle = messageSubTitle,
            buttonText = buttonText,
            listener = listener
        )
    }

    protected fun setStatusBarColor(color: Int, activity: Activity)
    {
        // Set Top Back ground to Status Bar
        val window: Window = activity.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(activity, color)
    }

}

// Extension for data-binding and view-binding START

fun <T : ViewBinding> Fragment.viewBinding(bind: (View) -> T): ReadOnlyProperty<Fragment, T> =
    object : ReadOnlyProperty<Fragment, T>
    {
        @Suppress("UNCHECKED_CAST")
        override fun getValue(thisRef: Fragment, property: KProperty<*>): T
        {
            (requireView().getTag(property.name.hashCode()) as? T)?.let { return it }
            return bind(requireView()).also {
                requireView().setTag(property.name.hashCode(), it)
            }
        }
    }

fun <T : ViewDataBinding> Fragment.dataBinding(): ReadOnlyProperty<Fragment, T>
{
    return object : ReadOnlyProperty<Fragment, T>
    {
        @Suppress("UNCHECKED_CAST")
        override fun getValue(thisRef: Fragment, property: KProperty<*>): T
        {
            (requireView().getTag(property.name.hashCode()) as? T)?.let { return it }
            return bind<T>(requireView()).also {
                it.lifecycleOwner = thisRef.viewLifecycleOwner
                it.root.setTag(property.name.hashCode(), it)
            }
        }

        private fun <T : ViewDataBinding> bind(view: View): T = DataBindingUtil.bind(view)!!
    }


}
