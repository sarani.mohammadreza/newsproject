package com.ms.newsApp.base.recyclerview

interface BaseActionAdapter<E>
{
    fun beforeBind(holder : E)
}