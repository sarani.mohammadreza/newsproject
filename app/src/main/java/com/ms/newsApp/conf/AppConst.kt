package com.ms.newsApp.conf

class AppConst
{
    class BackPressedBus
    {
        companion object
        {
            const val NAVIGATION_OPENED = 0
            const val NAVIGATION_CLOSE  = 1
        }
    }
}