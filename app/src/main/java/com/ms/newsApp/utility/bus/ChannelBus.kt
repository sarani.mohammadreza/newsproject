package com.ms.newsApp.utility.bus

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.*


@Suppress("UNCHECKED_CAST")
@ExperimentalCoroutinesApi
open class ChannelBus {

    private val channels = mutableMapOf<Class<*>, BroadcastChannel<Any>>()

    internal fun <T : Any> forEvent(clazz: Class<T>): BroadcastChannel<T> {
        return channels.getOrPut(clazz) {
            ConflatedBroadcastChannel()
        } as BroadcastChannel<T>
    }

    @JvmOverloads
    fun <T : Any> post(event: T, retain: Boolean = true) {
        val channel = forEvent(event.javaClass)
        channel.offer(event).also {
            if (!it)
                throw IllegalStateException("ConflatedChannel cannot take element, this should never happen")
        }
        if (!retain)
            dropEvent(event.javaClass)
    }

    fun <T : Any> getLastEvent(clazz: Class<T>): T? {
        val channel = channels.getOrElse(clazz) { null }
        val value = (channel as? ConflatedBroadcastChannel<Any>)?.valueOrNull
        return value as? T
    }

    suspend fun <T : Any> awaitEvent(clazz: Class<T>, skipRetained: Boolean = false): T {
        val receiveChannel = forEvent(clazz).openSubscription()

        if (skipRetained)
            receiveChannel.poll()

        while (true) {
            val received = receiveChannel.receive().takeUnless { it is DummyEvent }
            return received ?: continue
        }
    }

    fun dropEvent(clazz: Class<*>) {
        if (!channels.contains(clazz)) return
        val channel = channels[clazz] as BroadcastChannel<Any>
        channel.offer(DummyEvent())
    }

    fun dropAll() {
        channels.forEach {
            it.value.offer(DummyEvent())
        }
    }
}