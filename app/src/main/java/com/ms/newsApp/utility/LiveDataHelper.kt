package com.ms.newsApp.utility

import androidx.annotation.MainThread
import androidx.lifecycle.*
import java.util.concurrent.atomic.AtomicBoolean


fun <T> LifecycleOwner.observe(liveData: LiveData<T>, action: (t: T) -> Unit)
{
    liveData.observe(this , Observer { it?.let { t -> action(t) } })
}

fun <T> LifecycleOwner.observe(liveData: MutableLiveData<T>, action: (t: T) -> Unit)
{
    liveData.observe(this , Observer { it?.let { t -> action(t) } })
}

fun <T> LifecycleOwner.observe(liveData: SingleLiveEvent<T>, action: (t: T) -> Unit)
{
    liveData.observe(this , Observer {it?.let { t -> action(t) }})
}

class SingleLiveEvent<T>(): MutableLiveData<T>()
{
    private val pending = AtomicBoolean(false)

    companion object
    {
        private const val TAG = "SingleLiveEvent"
    }

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>)
    {
        if (hasActiveObservers())
        {
            L.e(
                TAG,
                "Multiple observers registered but only one will be notified of changes."
            )
        }
        else
        {
            L.e(
                TAG,
                TAG
            )
        }
        super.observe(owner, { t ->
            if (pending.compareAndSet(true, false))
            {
                observer.onChanged(t)
            }
        })
    }

    @MainThread
    override fun setValue(t: T?)
    {
        pending.set(true)
        super.setValue(t)
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    @MainThread
    fun call()
    {
        value = null
    }

}