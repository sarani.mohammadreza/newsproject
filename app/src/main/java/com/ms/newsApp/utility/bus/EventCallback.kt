package com.ms.newsApp.utility.bus

interface EventCallback<T> {

    fun onEvent(event: T)
}
