package com.ms.newsApp.utility.bus

import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
object GlobalBus : ChannelBus()