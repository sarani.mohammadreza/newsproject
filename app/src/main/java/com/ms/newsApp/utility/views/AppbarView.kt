package com.ms.newsApp.utility.views

import android.content.Context
import android.graphics.Typeface
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.ms.newsApp.R
import com.ms.newsApp.utility.UtilityHelper

class AppbarView(private val builder: Builder)
{
    private var context: Context
    private var tvTitle: TextView
    private var ivBack: AppCompatImageView
    private var ivMenu: AppCompatImageView
    //private var root : AppBarLayout

    class Builder(private val view: View)
    {
        private var title = ""
        private var textSize = -1
        private var textColor = -1
        private var titleFont = TitleFont.REGULAR
        private var onBackClicked: ClickListener.BackClick? = null
        private var onMenuClicked: ClickListener.MenuClick? = null
        private var backVisibility: Boolean = true

        private var menuVisible: Boolean = false
        private var backgroundResId: Int = -1
        private var backIcon = -1
        private var menuIcon = -1
        private var menuTintIcon = -1

        internal fun getView() = view


        fun setMenuTintIcon(@ColorInt id: Int): Builder
        {
            this.menuTintIcon = id
            return this
        }

        fun getMenuTintColor() = menuTintIcon

        fun setBackIcon(@DrawableRes id: Int): Builder
        {
            this.backIcon = id
            return this
        }

        internal fun getBackIcon() = backIcon


        fun setMenuIcon(@DrawableRes id: Int): Builder
        {
            this.menuIcon = id
            return this
        }

        internal fun getMenuIcon() = menuIcon

        fun setBackground(@DrawableRes id: Int): Builder
        {
            this.backgroundResId = id
            return this
        }

        internal fun getBackground() = backgroundResId


        fun setMenuVisible(visibility: Boolean): Builder
        {
            this.menuVisible = visibility
            return this
        }

        internal fun getMenuVisible() = menuVisible

        fun setTextColor(@ColorInt color: Int): Builder
        {
            this.textColor = color
            return this
        }

        internal fun getTextColor() = textColor

        fun setTitleText(title: String): Builder
        {
            this.title = title
            return this
        }

        internal fun getTitleText() = title


        internal fun getTextSize() = textSize

        fun setTitleFont(font: TitleFont): Builder
        {
            this.titleFont = font
            return this
        }

        internal fun getTitleFont() = titleFont

        fun setOnBackClickListener(clicked: ClickListener.BackClick): Builder
        {
            this.onBackClicked = clicked
            return this
        }

        internal fun getOnBackClickListener() = onBackClicked

        fun setOnMenuClickListener(clicked: ClickListener.MenuClick): Builder
        {
            this.onMenuClicked = clicked
            return this
        }

        internal fun getOnMenuClickListener() = onMenuClicked


        internal fun getBackVisibility() = backVisibility

        fun setBackVisibility(visibility: Boolean): Builder
        {
            this.backVisibility = visibility
            return this
        }


        fun build(): AppbarView
        {
            return AppbarView(this)
        }
    }

    init
    {
        this.context = builder.getView().context
        // init Views
        tvTitle = builder.getView().findViewById(R.id.tvTitle)
        ivBack = builder.getView().findViewById(R.id.ivBack)
        ivMenu = builder.getView().findViewById(R.id.ivMenu)
        // root = builder.getView().findViewById(R.id.appbarRoot) !!

        if (UtilityHelper.stringNotNull(builder.getTitleText()))
            tvTitle.text = builder.getTitleText()

        if (builder.getTextSize() != -1)
            tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, builder.getTextSize().toFloat())

        if (builder.getMenuVisible())
            ivMenu.visibility = View.VISIBLE

        if (builder.getBackVisibility())
            ivBack.visibility = View.VISIBLE

        if (builder.getTextColor() != -1)
            tvTitle.setTextColor(ContextCompat.getColor(context, builder.getTextColor()))

        if (builder.getBackground() != -1)
        {
            builder.getView().background =
                ContextCompat.getDrawable(context, builder.getBackground())

            ivBack.setColorFilter(ContextCompat.getColor(context, R.color.textColorSubMenu))
            // send different icon instead changing the color
//            ivMenu.setColorFilter(ContextCompat.getColor(context, R.color.textColorSubMenu))
        }
        if (builder.getBackIcon() != -1)
            ivBack.setImageDrawable(ContextCompat.getDrawable(context, builder.getBackIcon()))

        if (builder.getMenuIcon() != -1)
            ivMenu.setImageDrawable(ContextCompat.getDrawable(context, builder.getMenuIcon()))

        if (builder.getMenuTintColor() != -1)
            ivMenu.setColorFilter(ContextCompat.getColor(context, builder.getMenuTintColor()))



        when (builder.getTitleFont())
        {
            TitleFont.REGULAR ->
            {
                //  tvTitle.setTypeface(tvTitle.typeface, Typeface.DEFAULT)
            }
            TitleFont.MEDIUM ->
            {
                tvTitle.setTypeface(tvTitle.typeface, Typeface.NORMAL)
            }
            TitleFont.BOLD ->
            {
                tvTitle.setTypeface(tvTitle.typeface, Typeface.BOLD)
            }
        }
        if (builder.getOnBackClickListener() != null)
            ivBack.setOnClickListener {
                builder.getOnBackClickListener()!!.onBackClick(it)
            }

        if (builder.getOnMenuClickListener() != null)
            ivMenu.setOnClickListener {
                builder.getOnMenuClickListener()!!.onMenuClick(it)
            }

    }

    fun changeBackButtonActivity(active: Boolean)
    {
        if (active)
        {
            ivBack.alpha = 0.4F
        } else
        {
            ivBack.alpha = 1F
        }
    }

    fun changeMenuActivity(active: Boolean)
    {
        if (active)
        {
            ivMenu.alpha = 0.4F
        } else
        {
            ivMenu.alpha = 1F
        }
    }

    fun changeBackClickable(clickable: Boolean)
    {
        ivBack.isClickable = clickable
    }

    fun changeMenuClickable(clickable: Boolean)
    {
        ivMenu.isClickable = clickable
    }

    enum class TitleFont
    {
        BOLD,
        REGULAR,
        MEDIUM
    }

    interface ClickListener
    {
        interface BackClick
        {
            fun onBackClick(v: View)
        }

        interface MenuClick
        {
            fun onMenuClick(v: View)
        }


    }

}