package com.ms.newsApp.utility.views

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.airbnb.lottie.LottieAnimationView
import com.ms.newsApp.utility.UtilityHelper
import kotlinx.android.synthetic.main.v_custom_button.view.*


class CustomButton(val builder: Builder)
{
    private var context: Context
    private var tv: TextView
    private var icon: AppCompatImageView
    private var root: View
    private var loadingBar: LottieAnimationView
    
    class Builder(private val view: View)
    {
        private var title = ""
        private var gravity = - 1
        private var backgroundResId = - 1
        private var backgroundColorId = - 1
        private var textColorId = - 1
        private var textSize = - 1
        private var buttonFont = ButtonFont.REGULAR
        private var onClickListener: OnButtonClickListener? = null
        
        private var icon = - 1
        private var iconTintColor = - 1
        private var iconVisibility = true
        
        private var rootBackground = -1
        private var loading = false
        
        fun getLoadingVisibility() = loading
        
        fun setLoadingVisibility(loading: Boolean): Builder
        {
            this.loading = loading
            return this
        }
        
        fun getRootBackground() = rootBackground
        
        fun setRootBackground(@DrawableRes back : Int): Builder
        {
            this.rootBackground = back
            return this
        }
        
        fun getIconVisibility() = iconVisibility
        
        fun setIconVisibility(visibility: Boolean): Builder
        {
            this.iconVisibility = visibility
            return this
        }
        
        
        fun getIconTintColor() = iconTintColor
        
        fun setIconTintColor(tint: Int): Builder
        {
            this.iconTintColor = tint
            return this
        }
        
        fun getIcon() = icon
        
        fun setIcon(icon: Int): Builder
        {
            this.icon = icon
            return this
        }
        
        fun getView() = view
        
        fun setTitleText(title: String): Builder
        {
            this.title = title
            return this
        }
        
        fun getTitleText() = title
        
        fun setGravity(gravity: Int): Builder
        {
            this.gravity = gravity
            return this
        }
        
        fun getGravity() = gravity
        
        fun setBackgroundResourcesId(id: Int): Builder
        {
            this.backgroundResId = id
            return this
        }
        
        fun getBackgroundResourcesId() = backgroundResId
        
        fun setBackgroundColorId(id: Int): Builder
        {
            this.backgroundColorId = id
            return this
        }
        
        fun getBackgroundColorId() = backgroundColorId
        
        fun setTextColorId(id: Int): Builder
        {
            this.textColorId = id
            return this
        }
        
        fun getTextColorId() = textColorId
        
        fun setTextSize(size: Int): Builder
        {
            this.textSize = size
            return this
        }
        
        fun getTextSize() = textSize
        
        fun setButtonFont(font: ButtonFont): Builder
        {
            this.buttonFont = font
            return this
        }
        
        fun getButtonFont() = buttonFont
        
        fun setOnClickListener(listener: OnButtonClickListener): Builder
        {
            this.onClickListener = listener
            return this
        }
        
        fun getOnClickListener() = onClickListener
        
        fun build(): CustomButton
        {
            return CustomButton(this)
        }
    }
    
    init
    {
        this.context = builder.getView().context
        tv = builder.getView().viewButton
        icon = builder.getView().ivInside
        root = builder.getView().rootViewButtonWithImage
        loadingBar = builder.getView().loadingBar
        
        if (UtilityHelper.stringNotNull(builder.getTitleText()))
        {
            tv.text = builder.getTitleText()
        }
        if (builder.getGravity() != - 1)
        {
            tv.gravity = builder.getGravity()
        }
        if (builder.getBackgroundResourcesId() != - 1)
        {
            tv.setBackgroundResource(builder.getBackgroundResourcesId())
        }
        else if (builder.getBackgroundColorId() != - 1)
        {
            tv.setBackgroundResource(builder.getBackgroundColorId())
        }
        if (builder.getTextColorId() != - 1)
        {
            tv.setTextColor(ContextCompat.getColor(context, builder.getTextColorId()))
        }
        if (builder.getTextSize() != - 1)
        {
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, builder.getTextSize().toFloat())
        }
        
        if (builder.getIcon() != - 1)
            icon.setImageDrawable(ContextCompat.getDrawable(context, builder.getIcon()))
        
        if (! builder.getIconVisibility())
            icon.visibility = View.GONE
        
        if (!builder.getLoadingVisibility())
        {
            loadingBar.visibility = View.GONE
        }
        else
        {
            loadingBar.visibility = View.VISIBLE
        }
        
        if (builder.getIconTintColor() != - 1)
            icon.setColorFilter(ContextCompat.getColor(context, builder.getIconTintColor()))
        
           
        if (builder.getRootBackground() != - 1)
            root.background = ContextCompat.getDrawable(context, builder.getRootBackground())
        
        
        
        
        
        
        when (builder.getButtonFont())
        {
            ButtonFont.REGULAR ->
            {
            }
            ButtonFont.MEDIUM ->
            {
            }
            ButtonFont.BOLD ->
            {
            }
        }
        if (builder.getOnClickListener() != null)
        {
            builder.getView().setOnClickListener { s ->
                builder.getOnClickListener()?.onButtonClick(s)
                showLoading()
            }
        }
    }
    
    fun showLoading()
    {
        loadingBar.visibility = View.VISIBLE
        loadingBar.isClickable = false
    }
    
    fun hideLoading()
    {
        loadingBar.visibility = View.GONE
        loadingBar.isClickable = true
    }
    
    fun setBackground(@DrawableRes background: Int)
    {
        root.background = ContextCompat.getDrawable(context , background)
    }
    
    fun startAnim()
    {
        val anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
        anim.duration = 2000
        
        root.startAnimation(anim)
    }
    
    fun changeButtonActivity(active: Boolean)
    {
        if (active)
        {
            tv.alpha = 0.4F
        } else
        {
            tv.alpha = 1F
        }
    }
    
    fun changeClickable(clickable: Boolean)
    {
        builder.getView().isClickable = clickable
    }
    
    fun changeVisibility(visible:Boolean){
        if (visible){
            builder.getView().visibility=View.VISIBLE
        }else{
            builder.getView().visibility=View.GONE
        }
    }
    
    enum class ButtonFont
    {
        BOLD,
        REGULAR,
        MEDIUM
    }
    
    interface OnButtonClickListener
    {
        fun onButtonClick(v: View)
    }
}