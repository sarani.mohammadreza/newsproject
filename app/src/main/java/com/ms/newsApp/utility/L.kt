package com.ms.newsApp.utility

import android.annotation.SuppressLint
import android.util.Log
import com.ms.newsApp.BuildConfig

class L {
        companion object
        {
            private val ENABLE_LOGS: Boolean = BuildConfig.DEBUG

            fun v(tag: String?, msg: String?)
            {
                if (ENABLE_LOGS)
                {
                    Log.v(tag ?: "News", msg !!)
                }
            }


            fun v(tag: String?, msg: String?, e: Exception?)
            {
                if (ENABLE_LOGS)
                {
                    Log.v(tag ?: "News", msg, e)
                }
            }

            fun v(tag: String?, msg: String?, e: OutOfMemoryError?)
            {
                if (ENABLE_LOGS)
                {
                    Log.v(tag ?: "News", msg, e)
                }
            }

            fun d(tag: String?, msg: String?)
            {
                if (ENABLE_LOGS)
                {
                    Log.d(tag ?: "News", msg !!)
                }
            }

            fun d(tag: String?, msg: String?, e: Exception?)
            {
                if (ENABLE_LOGS)
                {
                    Log.d(tag ?: "News", msg, e)
                }
            }

            fun d(tag: String?, msg: String?, e: OutOfMemoryError?)
            {
                if (ENABLE_LOGS)
                {
                    Log.d(tag ?: "News", msg, e)
                }
            }

            fun e(tag: String?, msg: String?)
            {
                if (ENABLE_LOGS)
                {
                    Log.e(tag ?: "News", msg ?: "null")
                }
            }

            fun e(tag: String?, msg: String?, e: Exception?)
            {
                if (ENABLE_LOGS)
                {
                    Log.e(tag ?: "News", msg, e)
                }
            }

            @SuppressLint("LogNotTimber")
            fun e(tag: String?, msg: String?, e: OutOfMemoryError?)
            {
                if (ENABLE_LOGS)
                {
                    Log.e(tag ?: "News", msg, e)
                }
            }

            fun getIsLogsEnabled(): Boolean
            {
                return ENABLE_LOGS
            }

            fun reportException(e: Exception)
            {
                if (ENABLE_LOGS)
                {
                    Log.e("Exception", e.toString(), e)
                }
            }
        }

}