package com.ms.newsApp.utility.bus

import android.os.Looper
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ReceiveChannel


typealias EventReceiver = EventsReceiver

@ExperimentalCoroutinesApi
open class EventsReceiver @JvmOverloads constructor(
    private val bus: ChannelBus = GlobalBus
) {

    private val listeners = mutableListOf<EventListener>()

    private var returnDispatcher =
        if (Looper.myLooper() == Looper.getMainLooper())
            Dispatchers.Main
        else
            Dispatchers.Default

    fun returnOn(dispatcher: CoroutineDispatcher): EventsReceiver
    {
        returnDispatcher = dispatcher
        return this
    }

    @JvmOverloads
    fun <T : Any> subscribeTo(clazz: Class<T>, skipRetained: Boolean = false, callback: suspend (event: T) -> Unit): EventsReceiver
    {

        if (listeners.any { it.clazz == clazz })
            throw IllegalArgumentException("Already subscribed for event type: $clazz")

        val channel = bus.forEvent(clazz).openSubscription()
        val job = CoroutineScope(Job() + Dispatchers.Default).launch {

            if (skipRetained)
                channel.poll()

            while (isActive && !channel.isClosedForReceive) {
                val received = try {
                    channel.receive()
                } catch (ex: ClosedReceiveChannelException) {

                    return@launch
                }
                if (received is DummyEvent) continue

                withContext(returnDispatcher) { callback(received) }
            }
        }
        listeners.add(EventListener(clazz, job, channel))
        return this
    }

    @JvmOverloads
    fun <T : Any> subscribeTo(clazz: Class<T>, callback: EventCallback<T>, skipRetained: Boolean = false): EventsReceiver
    {
        return subscribeTo(clazz, skipRetained) { callback.onEvent(it) }
    }

    inline fun <reified T : Any> subscribe(skipRetained: Boolean = false, noinline callback: suspend (event: T) -> Unit): EventsReceiver
    {
        return subscribeTo(T::class.java, skipRetained, callback)
    }

    inline fun <reified T : Any> subscribe(callback: EventCallback<T>, skipRetained: Boolean = false): EventsReceiver
    {
        return subscribeTo(T::class.java, callback, skipRetained)
    }


    fun unsubscribeAll() {
        listeners.forEach { it.unsubscribe() }
        listeners.clear()
    }

    private class EventListener(
        val clazz: Class<*>,
        private val job: Job,
        private val channel: ReceiveChannel<*>
    ) {

        fun unsubscribe() {
            job.cancel()
            channel.cancel()
        }
    }
}