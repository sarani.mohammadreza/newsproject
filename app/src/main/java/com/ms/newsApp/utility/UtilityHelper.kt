package com.ms.newsApp.utility

class UtilityHelper
{
    companion object
    {
        fun stringNotNull(text: String?): Boolean
        {
            return text != null && text.isNotEmpty() && text != "null"
        }

        fun toPersian(n: Int): String
        {
            val p = "۰۱۲۳۴۵۶۷۸۹"
            return n.toString().trim().map {
                p[it.toString().trim().toInt()]
            }.joinToString()
        }

    }
}