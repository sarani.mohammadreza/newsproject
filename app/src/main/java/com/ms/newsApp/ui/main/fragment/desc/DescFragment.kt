package com.ms.newsApp.ui.main.fragment.desc

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.BounceInterpolator
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.ms.newsApp.R
import com.ms.newsApp.base.BaseFragment
import com.ms.newsApp.ui.main.MainActivity
import com.ms.newsApp.utility.views.CustomButton
import kotlinx.android.synthetic.main.fragment_item.*


class DescFragment(override val layoutId: Int = R.layout.fragment_item) : BaseFragment() {

    private lateinit var activity : MainActivity
    private var siteLink = ""
    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        this.activity = getActivity() as MainActivity
    }

    override fun initUI() {

        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)

        toolbar.setNavigationOnClickListener{
            Navigation.findNavController(it).navigateUp()
        }

        arguments?.let { s->
            val safeArgs = DescFragmentArgs.fromBundle(s)
            safeArgs.model.let {
                tvAuthorDescFragment.text = it.author
                tvContentDescFragment.text = it.description
                tvDateDescFragment.text = it.publishedAt


                siteLink = it.url.toString()

                Glide.with(activity).load(it.urlToImage).into(imgNews)

            }

        }

        CustomButton.Builder(btnGoToPage).setTitleText(getString(R.string.goToPage))
            .setBackgroundColorId(R.color.teal_700)
            .setOnClickListener(object : CustomButton.OnButtonClickListener
            {
                override fun onButtonClick(v: View)
                {
                    if (!siteLink.startsWith("http://$siteLink") && !siteLink.startsWith("https://"))
                        siteLink = "http://${siteLink}"

                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(siteLink)))
                }
            }).build()

        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = BounceInterpolator()
        fadeIn.duration = 750
        val animation = AnimationSet(true)
        animation.addAnimation(fadeIn)

        nestedRoot.startAnimation(animation)
    }

    override fun initListener() {

    }

}