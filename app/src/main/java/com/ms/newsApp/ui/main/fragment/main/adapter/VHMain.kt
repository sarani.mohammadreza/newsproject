package com.ms.newsApp.ui.main.fragment.main.adapter

import android.view.View
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ms.newsApp.ui.main.fragment.desc.model.DescModel
import com.ms.newsApp.ui.main.fragment.main.MainFragmentDirections
import com.ms.remote.model.NewsResponse
import kotlinx.android.synthetic.main.row_main.view.*

class VHMain(private val v: View) : RecyclerView.ViewHolder(v)
{
    fun setData(news: NewsResponse)
    {
        v.tvTitleNews.text = news.description
        v.tvDateNews.text = news.publishedAt
        v.tvGroupingNews.text = news.author

        v.loading.visibility = View.GONE

        Glide.with(v)
            .load(news.urlToImage)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(v.imgNews)

        v.root.setOnClickListener{
            news.let {
                val model = DescModel(DescModel.Source(it.source.id , it.source.name) , it.author , it.title , it.description , it.url , it.urlToImage , it.publishedAt , it.content)

                Navigation.findNavController(v).navigate(MainFragmentDirections.actionMainFragmentToDescFragment(model))
            }


        }

    }

    fun updateScore(item: NewsResponse?)
    {
        v.loading.visibility = View.VISIBLE
    }

}