package com.ms.newsApp.ui.main.fragment.main.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.ms.newsApp.R
import com.ms.remote.model.NewsResponse

class NewsRepoAdapter(private val context: Context) :
    PagedListAdapter<NewsResponse, VHMain>(NEWS_COMPARATOR)
{
    val sliderResponse = MutableLiveData<NewsResponse>()

    // For 1 Call
    private var postDataToMain = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHMain = VHMain(
        LayoutInflater.from(context).inflate(
            R.layout.row_main, parent, false
        )
    )

    override fun onBindViewHolder(holder: VHMain, position: Int)
    {
        val news = getItem(position)
        news?.let {
            holder.setData(news)
        }
        // items For Image Slider on Main Page
        if (!postDataToMain && position < 6)
            sliderResponse.value = news
        else
            postDataToMain = true
    }

    override fun onBindViewHolder(holder: VHMain, position: Int, payloads: MutableList<Any>)
    {
        if (payloads.isNotEmpty()) {
            val item = getItem(position)
            holder.updateScore(item)
        } else {
            onBindViewHolder(holder, position)
        }
    }

    companion object
    {
        private val NEWS_COMPARATOR = object : DiffUtil.ItemCallback<NewsResponse>()
        {
            override fun areItemsTheSame(oldItem: NewsResponse, newItem: NewsResponse): Boolean =
                oldItem.source.id == newItem.source.id

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: NewsResponse, newItem: NewsResponse): Boolean =
                oldItem == newItem

        }
    }
}