package com.ms.newsApp.ui.main.fragment.main.adapter

import com.ms.newsApp.base.BaseDataSource
import com.ms.newsApp.utility.L
import com.ms.remote.model.NewsResponse
import com.ms.remote.network.NetworkState
import com.ms.repository.InRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class NewsRepoDataSource(dataService: InRepository) : BaseDataSource<Int, NewsResponse>(dataService)
{
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, NewsResponse>
    )
    {
        CoroutineScope(Dispatchers.Main).launch {
            dataService.getNews().collect {
                when (it)
                {
                    is NetworkState.Success ->
                    {
                        it.let {
                            callback.onResult(it.response.articles, null, FIRST_PAGE + 1)
                        }
                    }

                    is NetworkState.Error ->
                    {
                    }
                    // Network State -> Empty State
                    else ->
                    {
                        L.e("Mohammadreza", "NotFoundDataSourcePage")
                    }
                }
            }
        }

    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, NewsResponse>)
    {
        CoroutineScope(Dispatchers.Main).launch {

            dataService.getNews(params.key , PAGE_SIZE).collect {
                when (it)
                {
                    is NetworkState.Success ->
                    {
                        it.let {
                            callback.onResult(
                                it.response.articles,
                                if (params.key > 1) params.key - 1 else 0
                            )
                        }
                    }

                    is NetworkState.Error ->
                    {
                    }
                    // Network State -> Empty State
                    else ->
                    {
                        L.e("Mohammadreza", "NotFoundDataSourcePage")
                    }
                }
            }
        }
    }

    // append Page With Key
    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, NewsResponse>)
    {
        CoroutineScope(Dispatchers.Main).launch {

            dataService.getNews(params.key , PAGE_SIZE).collect {
                when (it)
                {
                    is NetworkState.Success ->
                    {
                        it.let {
                            callback.onResult(
                                it.response.articles,
                                if (it.response.totalResults > params.key) params.key + 1 else 0
                            )
                        }
                    }

                    is NetworkState.Error ->
                    {
                    }
                    // Network State -> Empty State
                    else ->
                    {
                        L.e("Mohammadreza", "NotFoundDataSourcePage")
                    }
                }
            }
        }
    }


    companion object
    {
        const val PAGE_SIZE = 10
        const val FIRST_PAGE = 1
    }
}
