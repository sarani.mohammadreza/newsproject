package com.ms.newsApp.ui.main.fragment.main

import androidx.lifecycle.LiveData
import androidx.paging.ItemKeyedDataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ms.newsApp.base.BaseVM
import com.ms.newsApp.ui.main.fragment.main.adapter.NewsDataSourceFactory
import com.ms.newsApp.ui.main.fragment.main.adapter.NewsRepoDataSource
import com.ms.remote.model.NewsResponse
import com.ms.repository.InRepository

class MainVM(dataService: InRepository, private val itemDataSourceFactory : NewsDataSourceFactory) : BaseVM<MainRequest>(dataService)
{
    private val liveDataSource:LiveData<NewsRepoDataSource>
    val newsRepoPagedList : LiveData<PagedList<NewsResponse>>

    init
    {
        liveDataSource = itemDataSourceFactory.newsLiveData

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(NewsRepoDataSource.FIRST_PAGE)
            .build()

        newsRepoPagedList = LivePagedListBuilder(itemDataSourceFactory , config)
            .build()


    }


    fun refreshLayout (){}// = newsRepoPagedList.value?.dataSource?.invalidate()



/*    fun getNews() = viewModelScope.launch {
        getPresenter()?.showLoading()
    private val _repos = MutableLiveData<ArrayList<NewsResponse>>()
    var repos: LiveData<ArrayList<NewsResponse>> = _repos
        dataService.getNews().collect {
            when (it)
            {
                is NetworkState.Success ->
                {
                    getPresenter()?.hideLoading()

                    getPresenter()?.getNews(it.response.articles.size)
                    _repos.value = it.response.articles
                }

                is NetworkState.Error ->
                {
                    getPresenter()?.hideLoading()
                    getPresenter()?.showError(it.exception.code, it.exception.message)
                }
                // Network State -> Empty State
                else ->
                {
                    getPresenter()?.hideLoading()
                    L.e("Mohammadreza", "NotFound")
                }
            }
        }
    }*/

}