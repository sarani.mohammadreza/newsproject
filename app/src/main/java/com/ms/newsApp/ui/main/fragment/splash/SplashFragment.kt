package com.ms.newsApp.ui.main.fragment.splash

import android.content.Context
import android.view.View
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.ms.newsApp.R
import com.ms.newsApp.base.BaseFragment
import com.ms.newsApp.ui.main.MainActivity
import kotlinx.coroutines.*

class SplashFragment(override val layoutId: Int = R.layout.fragment_splash) : BaseFragment()
{
    private lateinit var activity: MainActivity

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        activity = getActivity() as MainActivity
    }

    override fun initUI()
    {
        // Hide Action Bar For Splash Fragment And Then Show it
        activity.supportActionBar?.hide()

        CoroutineScope(Dispatchers.Main).launch {
            delay(4000)
            // Remove Fragment After Splash Completed From Back Stack
            NavHostFragment.findNavController(this@SplashFragment)
                .popBackStack(R.id.splashFragment, true)
            NavHostFragment.findNavController(this@SplashFragment).navigate(R.id.mainFragment)
        }
    }

    override fun initListener()
    {
    }

}