package com.ms.newsApp.ui.main.mainDrawer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ms.newsApp.R
import com.ms.newsApp.ui.main.mainDrawer.adapter.SideNavAdapter
import com.ms.util.extensions.toast
import kotlinx.android.synthetic.main.fragment_side_nav.*

class SideNavFragment : Fragment(){

    private val sideNavAdapter: SideNavAdapter = SideNavAdapter {position, item ->
        onItemClick(position,item)
    }

    private fun onItemClick(position: Int, item: DrawerModel) {
        toast(""+item.itemName)
       // (activity as MenuScreen).closeDrawer(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?  = inflater.inflate(R.layout.fragment_side_nav, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    private fun setUpViews() {
        rv_side_nav_options?.adapter = sideNavAdapter
        sideNavAdapter.setData(prepareNavItems())
    }

    private fun prepareNavItems(): List<DrawerModel> {
        val menuItemsList = ArrayList<DrawerModel>()
        menuItemsList.add(DrawerModel(1,"Upgrade",R.drawable.ic_baseline_arrow_back_24))
        menuItemsList.add(DrawerModel(2,"Rate us",R.drawable.ic_baseline_menu_24))
        menuItemsList.add(DrawerModel(3,"Share",R.drawable.ic_launcher_background))
        menuItemsList.add(DrawerModel(5,"Settings",R.drawable.ic_baseline_settings_24))
        return menuItemsList
    }
}