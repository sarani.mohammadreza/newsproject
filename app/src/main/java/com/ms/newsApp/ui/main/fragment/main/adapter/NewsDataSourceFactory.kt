package com.ms.newsApp.ui.main.fragment.main.adapter

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.ms.remote.model.NewsResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class NewsDataSourceFactory(val dataSource: NewsRepoDataSource) : DataSource.Factory<Int, NewsResponse>()
{
    val newsLiveData = MutableLiveData<NewsRepoDataSource>()

    // Execute  To Create Data Source
    override fun create(): DataSource<Int, NewsResponse>
    {
        CoroutineScope(Dispatchers.Main).launch {
            newsLiveData.value = dataSource
        }
        return dataSource
    }
}