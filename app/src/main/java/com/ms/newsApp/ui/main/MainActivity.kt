package com.ms.newsApp.ui.main


import com.ms.newsApp.R
import com.ms.newsApp.base.BaseActivity
import com.ms.newsApp.conf.AppConst
import com.ms.newsApp.utility.bus.EventsReceiver
import com.ms.newsApp.utility.bus.GlobalBus
import kotlinx.coroutines.ExperimentalCoroutinesApi

class MainActivity(override val layoutId: Int = R.layout.activity_main) : BaseActivity()
{

    @ExperimentalCoroutinesApi
    private val receiver by lazy { EventsReceiver() }

    @ExperimentalCoroutinesApi
    override fun onBackPressed()
    {
        GlobalBus.post(AppConst.BackPressedBus.NAVIGATION_OPENED)

        receiver.subscribe<Int> {
            when(it)
            {
                AppConst.BackPressedBus.NAVIGATION_CLOSE ->
                {
                    return@subscribe
                }
            }
        }
        super.onBackPressed()
    }



}
