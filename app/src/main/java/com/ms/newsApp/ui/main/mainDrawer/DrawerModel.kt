package com.ms.newsApp.ui.main.mainDrawer

data class DrawerModel(
    var id:Int,
    var itemName:String,
    var resourceId:Int
)