package com.ms.newsApp.ui.main.mainDrawer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ms.newsApp.R
import com.ms.newsApp.base.recyclerview.BaseAdapter
import com.ms.newsApp.base.recyclerview.BaseVH
import com.ms.newsApp.ui.main.mainDrawer.DrawerModel
import kotlinx.android.synthetic.main.row_side_nav.view.*

class SideNavAdapter( private val onItemClick: ((position: Int, item: DrawerModel) -> Unit)) : BaseAdapter<DrawerModel , SideNavAdapter.SideVH>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SideVH = SideVH(LayoutInflater.from(parent.context).inflate(
        R.layout.row_side_nav, parent , false))

    inner class SideVH(itemView: View) : BaseVH<DrawerModel>(itemView)
    {
        override fun bind(item: DrawerModel, position: Int)
        {
            itemView.setOnClickListener {
                    onItemClick.invoke(adapterPosition, item)
            }
            itemView.iv_nav_option?.setImageResource(item.resourceId)
            itemView.tv_nav_text?.text = item.itemName
        }
    }

}