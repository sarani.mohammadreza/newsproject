package com.ms.newsApp.ui.main.fragment.desc.model

import java.io.Serializable

data class DescModel
    (
    val source: Source,

    val author: String? = null,

    val title: String? = null,

    val description: String? = null,

    val url: String? = null,

    val urlToImage: String? = null,

    val publishedAt: String? = null,

    val content: String? = null
) : Serializable
{
    data class Source(

        val id: String? = null,

        val name: String? = null
    ): Serializable
}