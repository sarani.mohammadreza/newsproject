package com.ms.newsApp.ui.main.fragment.main

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.view.GravityCompat
import com.glide.slider.library.SliderLayout
import com.glide.slider.library.animations.DescriptionAnimation
import com.glide.slider.library.slidertypes.DefaultSliderView
import com.ms.newsApp.R
import com.ms.newsApp.base.BaseFragment
import com.ms.newsApp.conf.AppConst
import com.ms.newsApp.ui.main.MainActivity
import com.ms.newsApp.ui.main.fragment.main.adapter.NewsRepoAdapter
import com.ms.newsApp.utility.views.AppbarView
import com.ms.newsApp.utility.bus.EventsReceiver
import com.ms.newsApp.utility.bus.GlobalBus
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainFragment(override val layoutId: Int = R.layout.fragment_main) : BaseFragment(),
    MainRequest
{
    private lateinit var activity: MainActivity

    private val vm by viewModel<MainVM>()


    @ExperimentalCoroutinesApi
    private val receiver by lazy { EventsReceiver() }

    // just For Showing Loading
    override fun initPresenter() = vm.setP(this)


    @ExperimentalCoroutinesApi
    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        activity = getActivity() as MainActivity
        backClicked()
    }


    @SuppressLint("ResourceAsColor")
    override fun initUI()
    {
        // Init Appbar Layout On Main Page
        AppbarView.Builder(appbarMainFragment)
            .setBackIcon(R.drawable.ic_baseline_arrow_back_24)
            .setTitleText(activity.resources.getString(R.string.News))
            .setMenuVisible(true)
            .setBackVisibility(false)
            .setTitleFont(AppbarView.TitleFont.BOLD)
            .setTextColor(R.color.white)
            .setMenuTintIcon(R.color.white)
            .setOnBackClickListener(object : AppbarView.ClickListener.BackClick
            {
                override fun onBackClick(v: View)
                {
                    // This Method Is For Test Message Page
                }
            })
            .setOnMenuClickListener(object : AppbarView.ClickListener.MenuClick
            {
                override fun onMenuClick(v: View)
                {
                    // Drawer Menu
                    if (!drawer_layout.isOpen)
                        drawer_layout.openDrawer(GravityCompat.START)
                    else
                        drawer_layout?.closeDrawer(GravityCompat.START)

                }

            }).build()


        // Swipe Refresh Layout
        swipeRefreshMain.setOnRefreshListener {
            vm.refreshLayout()
            swipeRefreshMain.isRefreshing = false
        }

        // init adapter And Recycler View
        val aa = NewsRepoAdapter(activity)
        vm.newsRepoPagedList.observe(this, {
            // Submit Data To Adapter
            aa.submitList(it)
        })
        // set Adapter
        rvMain.adapter = aa


        // init Slider
        aa.sliderResponse.observe(this, {
            val txtSlider = DefaultSliderView(activity)
            txtSlider.image(it.urlToImage)
                .setProgressBarVisible(true)
                .description(it.description)
                .setOnSliderClickListener {
                }

            sliderLayout.setCustomIndicator(sliderIndicator)
            sliderLayout.addSlider(txtSlider)
        })


        // set animation to Slider With Some Configure
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion)

        sliderLayout.setCustomAnimation(DescriptionAnimation())

        sliderLayout.stopCyclingWhenTouch(false)

    }

    @ExperimentalCoroutinesApi
    override fun initListener()
    {
    }


    @ExperimentalCoroutinesApi
    private fun backClicked()
    {
        // use Integer for BackPressed
        receiver.subscribe<Int> {
            when (it)
            {
                AppConst.BackPressedBus.NAVIGATION_OPENED ->
                {
                    if (drawer_layout.isOpen)
                    {
                        drawer_layout.close()
                        GlobalBus.post(AppConst.BackPressedBus.NAVIGATION_CLOSE , true)
                    }
                }

            }
        }

    }

    override fun onStop()
    {
        sliderLayout.stopAutoCycle()
        super.onStop()
    }
}