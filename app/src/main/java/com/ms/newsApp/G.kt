package com.ms.newsApp

import com.ms.newsApp.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import androidx.multidex.MultiDexApplication
import timber.log.Timber

class G  : MultiDexApplication()
{
    override fun onCreate() {
        super.onCreate()
        initDi()
        initTimber()

    }

    private fun initDi()  = startKoin {
        if (BuildConfig.DEBUG)
            androidLogger()
        androidContext(this@G)
 //      androidFileProperties()
        modules(appModule)
    }

    private fun initTimber() {
        Timber.plant(Timber.DebugTree())
    }
}