package com.ms.newsApp.di

import com.ms.local.di.localModule
import com.ms.newsApp.BuildConfig
import com.ms.remote.di.remoteModule
import com.ms.repository.di.repositoryModule

val appModule = listOf(
    remoteModule(BuildConfig.BASE_URL , isDebug = BuildConfig.DEBUG) ,
    localModule(BuildConfig.DB_NAME),
    repositoryModule,
    viewModelModule(),
    adapterModule()
)