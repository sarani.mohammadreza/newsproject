package com.ms.newsApp.di


import com.ms.newsApp.ui.main.fragment.main.adapter.NewsDataSourceFactory
import com.ms.newsApp.ui.main.fragment.main.adapter.NewsRepoDataSource

import org.koin.dsl.module

fun adapterModule () = module {

    single {
        NewsRepoDataSource(get())
    }

    single {
        NewsDataSourceFactory(get())
    }
}