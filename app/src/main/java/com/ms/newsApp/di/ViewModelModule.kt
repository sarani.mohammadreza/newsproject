package com.ms.newsApp.di

import com.ms.newsApp.ui.main.fragment.main.MainVM
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

fun viewModelModule () = module {
    viewModel{ MainVM(get() , get()) }
}