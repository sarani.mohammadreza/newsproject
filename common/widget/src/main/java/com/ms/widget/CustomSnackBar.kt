package com.ms.widget

import android.annotation.SuppressLint
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.ms.common.widget.R

class CustomSnackBar {
    companion object
    {
        @SuppressLint("InflateParams")
        fun showSnackBar(snackBarType: SnackBarType,
                         containerLayout: ViewGroup,
                         messageTitle: String,
                         messageSubTitle: String? = null,
                         buttonText: String? = null,
                         listener: OnSnackBarActionListener? = null)
        {
            val snackBar = Snackbar.make(containerLayout, "", Snackbar.LENGTH_LONG)
            val layout = snackBar.view as Snackbar.SnackbarLayout
            // Hide the text
            val textView = layout.findViewById<View>(R.id.snackbar_text) as TextView
            textView.visibility = View.INVISIBLE

            // Inflate our custom view
            val snackView: View = LayoutInflater.from(containerLayout.context).inflate(R.layout.v_snackbar,
                null)
            // Configure the view
            val snackBarProgress : ProgressBar = snackView.findViewById(R.id.snackBarProgress) as ProgressBar
            val duration = 2750L

            var i = 0
            object: CountDownTimer(duration, 20L)
            {
                override fun onTick(millisUntilFinished: Long)
                {
                    i ++
                    snackBarProgress.progress = 100 - (millisUntilFinished * 100 / duration).toInt()
                }

                override fun onFinish()
                {
                    snackBarProgress.progress = 100
                }

            }.start()

            val snackBarTitle: TextView = snackView.findViewById(R.id.snackBarTitle) as TextView
            snackBarTitle.text = messageTitle

            val snackBarSubTitle: TextView = snackView.findViewById(R.id.snackBarSubTitle) as TextView
            if (messageSubTitle == null) snackBarSubTitle.visibility = View.GONE else snackBarSubTitle.text = messageSubTitle

            val snackBarActionButton: Button = snackView.findViewById(R.id.snackBarActionButton) as Button
            snackBarActionButton.text = buttonText ?: "Undo"

            if (listener == null) snackBarActionButton.visibility = View.INVISIBLE

            snackBarActionButton.setOnClickListener {

                snackBar.dismiss()
                listener!!.onSnackBarButtonActionClick()
            }

            //If the view is not covering the whole snackbar layout, add this line
            layout.setPadding(0, 0, 0, 0)

            // Add the view to the Snackbar's layout
            layout.addView(snackView, 0)

            when (snackBarType)
            {
                SnackBarType.SUCCESS -> layout.setBackgroundColor(
                    ContextCompat.getColor(
                        containerLayout.context,
                        R.color.snackBarSuccess))

                SnackBarType.ERROR -> layout.setBackgroundColor(
                    ContextCompat.getColor(
                        containerLayout.context,
                        R.color.snackBarFailure))

                SnackBarType.REGULAR ->
                {

                }
            }

            snackBar.show()
        }
    }

    interface OnSnackBarActionListener
    {
        fun onSnackBarButtonActionClick()
    }
}

enum class SnackBarType
{
    REGULAR, // default color
    ERROR, // red color
    SUCCESS // green color
}

