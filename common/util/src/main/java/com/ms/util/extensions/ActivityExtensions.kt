package com.ms.util.extensions

import android.app.Activity
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}


fun Activity.longToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Fragment.longToast(m : String)
{
    Toast.makeText(requireContext(), m, Toast.LENGTH_LONG).show()
}

fun Fragment.toast(m : String)
{
    Toast.makeText(requireContext(), m, Toast.LENGTH_SHORT).show()
}