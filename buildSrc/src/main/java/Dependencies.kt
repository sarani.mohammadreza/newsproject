object Modules
{
    const val app = ":app"
    const val remote = ":core:remote"
    const val local = ":core:local"
    const val repository = ":core:repository"
    const val util = ":common:util"
    const val widget = ":common:widget"

}

object ApplicationId
{
    const val id = "com.ms.newsApp"
}

object SignConfig
{
    const val keyAlias = "MS123"
    const val keyPassword = "123456789"
    const val storePassword = "123456789"
}

object Versions
{
    const val minSdk = 21
    const val compileSdk = 30
    const val targetSdk = 30
    const val buildTools = "30.0.2"
    const val kotlinPlugin = "1.4.30"
    const val androidPlugin = "4.1.2"

    const val navigation = "2.3.3"
    const val appCompat = "1.2.0"
    const val coreKtx = "1.3.2"
    const val constraintLayout = "2.0.4"
    const val material = "1.2.1"
    const val preference = "1.1.0"
    const val legacy = "1.0.0"


    const val paging = "2.1.2"

    const val coroutines = "1.3.9"

    // important coroutines with retrofit only get directly response min 2.6.0
    const val retrofit = "2.9.0"
    const val okhttp = "4.8.1"
    const val moshi = "2.9.0"
    const val mockWebServer = "3.12.6"
    const val gson = "2.8.6"
    const val chunk = "1.1.0"

    const val multidex = "2.0.1"

    const val lifecycle = "2.2.0"
    const val lifeCycleKtx = "2.2.0"

    const val koin = "2.0.1"

    const val room = "2.2.6"

    const val glide = "4.10.0"
    const val picasso = "2.71828"

    const val lottie = "3.0.7"

    const val junit = "4.13"
    const val junitExt = "1.1.1"
    const val espresso = "3.2.0"

    const val retrofitCoroutines = "0.9.2"
    const val robolectric = "4.3.1"
    const val timber = "4.7.1"

    const val picasso_slider = "1.1.5@aar"
    const val picasso_slider_dep = "2.4.0"


    const val glide_slider = "1.5.1"

}

object Libraries
{
    // kotlin - coroutine
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlinPlugin}"
    const val kotlinCoroutineCore =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
    const val kotlinCoroutineAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    const val kotlinReflection = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlinPlugin}"

    // android
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val legacy = "androidx.legacy:legacy-support-v4:${Versions.legacy}"
    const val dynamicAnimation = "androidx.dynamicanimation:dynamicanimation:${Versions.legacy}"
    const val preference = "androidx.preference:preference:${Versions.preference}"

    // paging Library
    const val paging = "androidx.paging:paging-runtime-ktx:${Versions.paging}"

    // multiDex
    const val multidex = "androidx.multidex:multidex:${Versions.multidex}"

    // dataBinding
    const val databinding = "com.android.databinding:compiler:${Versions.androidPlugin}"


    // glide for image
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"

    // picasso
    const val picasso = "com.squareup.picasso:picasso:${Versions.picasso}"

    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

    // room database for local
    const val room = "androidx.room:room-runtime:${Versions.room}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
    const val roomKtx = "androidx.room:room-ktx:${Versions.room}"

    // koin for di
    const val koinCore = "org.koin:koin-core:${Versions.koin}"
    const val koinAndroid = "org.koin:koin-android:${Versions.koin}"
    const val koinScope = "org.koin:koin-androidx-scope:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"

    // lifecycle
    const val lifeCycleLiveData = "androidx.lifecycle:lifecycle-livedata:${Versions.lifecycle}"
    const val lifeCycleViewModel = "androidx.lifecycle:lifecycle-viewmodel:${Versions.lifecycle}"
    const val lifeCycleExtension = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"

    const val lifeCycleLiveDataKtx =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifeCycleKtx}"
    const val lifeCycleRunTimeKtx =
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifeCycleKtx}"
    const val lifeCycleViewModelKtx =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val lifeCycleCommon = "androidx.lifecycle:lifecycle-common-java8:${Versions.lifecycle}"

    // retrofit
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitConvertorGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val retrofitCoroutineAdapter =
        "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${Versions.retrofitCoroutines}"

    const val moshi = "com.squareup.retrofit2:converter-moshi:${Versions.moshi}"

    const val chuck_debug = "com.readystatesoftware.chuck:library:${Versions.chunk}"
    const val chuck_release = "com.readystatesoftware.chuck:library-no-op:${Versions.chunk}"

    // okHttp
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
    const val okhttpLogging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"

    // navigation
    const val navFragment = "androidx.navigation:navigation-fragment:${Versions.navigation}"
    const val navUi = "androidx.navigation:navigation-ui:${Versions.navigation}"

    const val navFragmentKtx = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navUiKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"

    // Lottie
    const val lottie = "com.airbnb.android:lottie:${Versions.lottie}"

    // Custom Picasso Image Slider
    const val picasso_slider = "com.daimajia.slider:library:${Versions.picasso_slider}"
    const val picasso_slider_dep = "com.nineoldandroids:library:${Versions.picasso_slider_dep}"

    // Custom Glide Image Slider
    const val glider_slider = "com.github.firdausmaulan:GlideSlider:${Versions.glide_slider}"


    // test
    const val koinTest = "org.koin:koin-test:${Versions.koin}"
    const val lifeCycleTest = "androidx.arch.core:core-testing:${Versions.lifecycle}"
    const val junit = "junit:junit:${Versions.junit}"
    const val junitExt = "androidx.test.ext:junit:${Versions.junitExt}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val mockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.mockWebServer}"
    const val coroutineTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines}"
    const val roboelectric = "org.robolectric:robolectric:${Versions.robolectric}"
}

object BuildPlugins
{
    const val jitpack = "https://jitpack.io"

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.androidPlugin}"
    const val kotlinGradlePlugin =
        "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinPlugin}"
    const val safeArgsGradlePlugin =
        "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"

    const val supportLibraryVectorDrawables = true
    const val multidexEnable = true
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
}